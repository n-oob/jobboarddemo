/* eslint-disable import/no-anonymous-default-export */
import React from 'react';
import ReactDOM from 'react-dom';
import faker from 'faker'

export default class extends React.Component{

    render(){
        return (
            <div className = "job-brief">
                <h1>
                    {this.props.info.name}
                </h1>

                <h3>
                    {this.props.info.location.city}, {this.props.info.location.country}
                </h3>

                <img src = {this.props.info.logo} alt = "business stuff"/>

                <p className = "description">
                    {this.props.info.description}
                </p>

                <p className = "salary">
                ₹{this.props.info.salary}/month
                </p>

                <span className = "save"> <a href = "#">Save Job</a> </span>
                <span className = "more"> <a href = "#">More</a> </span>
            </div>
        );
    }
}